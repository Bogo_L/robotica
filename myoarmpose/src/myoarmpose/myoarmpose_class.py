# coding=utf-8

import rospy
from rosmyo.msg import ClassifierPose
from std_msgs.msg import ColorRGBA
from sensor_msgs.msg import Imu
from geometry_msgs.msg import Vector3, Quaternion
from visualization_msgs.msg import MarkerArray, Marker
from tf.transformations import *
from naoqi_bridge_msgs.msg import JointAnglesWithSpeed
import math
from collections import deque
import numpy
from myoarmpose.msg import Euler


# Transform Quaternion to iterable
def quat_to_iterable(q):
	return q.x, q.y, q.z, q.w


def vect_norm(v):
	return math.sqrt(sum([t ** 2 for t in vect_to_iterable(v)]))


def vect_to_iterable(v):
	return v.x, v.y, v.z


def vect_normalize(v):
	norm = vect_norm(v)
	return tuple(t / norm for t in v)


# rotate vector v by quaternion q
def vect_rotation(v, q):
	# Oggetti iterabili
	vi = [v.x, v.y, v.z]
	
	# Matrice di rotazione dal quaterione
	rotm = [row[:3] for row in quaternion_matrix(q)[:3]]
	
	# Vettore ruotato
	rv = [sum([row[i] * vi[i] for i in range(len(row))]) for row in rotm]
	
	return Vector3(*rv)


def default_marker():
	m = Marker()
	m.header.stamp = rospy.Time.now()
	m.header.frame_id = 'torso'
	m.ns = 'basic_shapes'
	m.id = 0
	m.type = Marker.ARROW
	m.action = Marker.ADD
	m.pose.position = Vector3(0, 0, 0)
	m.pose.orientation = Quaternion(0, 0, 0, 1)
	m.scale = Vector3(x=1.0, y=0.05, z=0.1)
	m.color = ColorRGBA(1.0, 1.0, 0.0, 1.0)
	return m


class MyoArmPose(object):
	# Costanti
	NODE_DEFAULT_NAME = 'myoarmpose'
	TOPIC_QUEUE_SIZE = 10
	
	def __init__(self):
		# Inizializzazione ROS
		rospy.init_node(MyoArmPose.NODE_DEFAULT_NAME, disable_signals=False)
		
		# Parametri
		arm_imu_topic = rospy.get_param('~arm_imu_topic', '/myos/arm/imu')
		forearm_imu_topic = rospy.get_param('~forearm_imu_topic', '/myos/forearm/imu')
		forearm_pose_topic = rospy.get_param('~forearm_pose_topic', '/myos/forearm/pose')
		markers_topic = rospy.get_param('~markers_topic', '/markers')
		eulers_topic = rospy.get_param('~eulers_topic', '/eulers')
		nao_joints_topic = rospy.get_param('~nao_joints_topic', '/nao_robot/pose/joint_angles')
		
		# Subscribers
		self.__sub_arm_imu = rospy.Subscriber(arm_imu_topic, Imu, self.sub_arm_imu, queue_size=MyoArmPose.TOPIC_QUEUE_SIZE)
		self.__sub_forearm_imu = rospy.Subscriber(forearm_imu_topic, Imu, self.sub_forearm_imu, queue_size=MyoArmPose.TOPIC_QUEUE_SIZE)
		self.__sub_forearm_pose = rospy.Subscriber(forearm_pose_topic, ClassifierPose, self.sub_forearm_pose)
		
		# Orientazione con offset iniziale
		self.__arm_orientation = [0, 0, 0, 1]
		self.__arm_orientation_offset = None
		self.__forearm_orientation = [0, 0, 0, 1]
		self.__forearm_orientation_offset = None
		
		# Pubblicazione angoli
		self.__pub_joints = rospy.Publisher(nao_joints_topic, JointAnglesWithSpeed, queue_size=MyoArmPose.TOPIC_QUEUE_SIZE)
		nao_arm = rospy.get_param('~nao_arm', 'right')
		if nao_arm.lower() == 'left':
			self.__joint_names = ['LShoulderRoll', 'LShoulderPitch', 'LElbowYaw', 'LElbowRoll', 'LWristYaw']
		else:
			self.__joint_names = ['RShoulderRoll', 'RShoulderPitch', 'RElbowYaw', 'RElbowRoll', 'RWristYaw']
		self.reset_arms()
		
		# Marker per debug
		self.__q_to_ny = quaternion_from_euler(0, 0, math.pi)
		self.__pub_markers = rospy.Publisher(markers_topic, MarkerArray, queue_size=MyoArmPose.TOPIC_QUEUE_SIZE)
		self.__pub_eulers = rospy.Publisher(eulers_topic, Euler, queue_size=MyoArmPose.TOPIC_QUEUE_SIZE)
		
	# Metodi interni
	
	def calculate_angles(self, qa, qf):
		# Calcolo la rotazione del braccio
		shoulder_yaw, shoulder_roll, shoulder_pitch = euler_from_quaternion(qa, 'sxzy')
		
		# Calcolo la rotazione relativa dell'avambraccio senza considerare il roll del braccio (lo otterrò con l'ElbowYaw)
		qa_no_yaw = quaternion_from_euler(0, shoulder_roll, shoulder_pitch, 'sxzy')
		qfa = quaternion_multiply(qf, quaternion_inverse(qa_no_yaw))
		wrist_yaw, elbow_roll, elbow_yaw = euler_from_quaternion(qfa, 'sxzx')
		
		e = Euler()
		e.roll = wrist_yaw * (180. / math.pi)
		e.pitch = elbow_roll * (180. / math.pi)
		e.yaw = elbow_yaw * (180. / math.pi)
		self.__pub_eulers.publish(e)
		
		q_x = 0, 0, 0, 1
		q_y = quaternion_from_euler(0, 0, math.pi / 2, 'sxyz')
		q_z = quaternion_from_euler(0, -math.pi / 2, 0, 'sxyz')
		
		# Assi dell'orientazione da cui calcolare ElbowYaw e ElbowRoll
		self.draw_markers(qa_no_yaw, quaternion_multiply(qa_no_yaw, q_x), 0, 1, arm_color=ColorRGBA(0.7, 0.7, 0.7, 1), forearm_color=ColorRGBA(1, 0.5, 0.5, 1), forearm_length=0.1)
		self.draw_markers(qa_no_yaw, quaternion_multiply(qa_no_yaw, q_y), 0, 2, arm_color=ColorRGBA(0.7, 0.7, 0.7, 1), forearm_color=ColorRGBA(0.5, 1, 0.5, 1), forearm_length=0.1)
		self.draw_markers(qa_no_yaw, quaternion_multiply(qa_no_yaw, q_z), 0, 3, arm_color=ColorRGBA(0.7, 0.7, 0.7, 1), forearm_color=ColorRGBA(0.5, 0.5, 1, 1), forearm_length=0.1)
		# Risultato finale desiderato
		self.draw_markers(qa_no_yaw, qf, 0, 4, arm_color=ColorRGBA(0.7, 0.7, 0.7, 1), forearm_color=ColorRGBA(1, 1, 0, 1), forearm_length=0.1)
		# Rotazione relativa dell'avambraccio sul braccio
		self.draw_markers(qfa, (0, 0, 0, 1), 5, 6, arm_color=ColorRGBA(1, 1, 0, 1), forearm_color=ColorRGBA(1, 1, 0, 0))
		
		# Se vuole girare il gomito di un angolo negativo inverto la rotazione del gomito (ElbowRoll)
		# e aggiungo 180° a quella "della spalla" (ElbowYaw) per riportarmi allo stesso punto finale
		if elbow_roll < 0:
			elbow_roll = -elbow_roll
			elbow_yaw = (elbow_yaw % (2 * math.pi)) - math.pi
		
		return shoulder_roll, shoulder_pitch, elbow_yaw, elbow_roll, 0
	
		# return shoulder_roll, shoulder_pitch, 0, 0, 0
	
	def publish_joints(self, names, angles):
		m = JointAnglesWithSpeed()
		m.header.stamp = rospy.Time.now()
		m.joint_names = names
		m.joint_angles = angles
		m.relative = 0
		m.speed = 1
		self.__pub_joints.publish(m)
		
	def reset_arms(self):
		self.publish_joints([
			'RShoulderRoll', 'RShoulderPitch', 'RElbowYaw', 'RElbowRoll', 'RWristYaw',
			'LShoulderRoll', 'LShoulderPitch', 'LElbowYaw', 'LElbowRoll', 'LWristYaw'
		], [0] * 10)
	
	# Subscribers
	
	def sub_forearm_pose(self, msg):
		# Se la posa è double-tap resetto l'offset
		if msg.pose == ClassifierPose.DOUBLE_TAP:
			self.__arm_orientation_offset = None
			self.__forearm_orientation_offset = None
	
	def sub_forearm_imu(self, msg):
		# Quaterione iterabile
		orientation = quat_to_iterable(msg.orientation)
		
		# Riporto la rotazione con X+ verso il polso e Y+ verso l'interno, invece che X+ verso la spalla e Y+ verso l'esterno
		orientation = quaternion_multiply(quaternion_multiply(self.__q_to_ny, orientation), self.__q_to_ny)
		
		# Verifico se e' stato acquisito l'offset
		if self.__forearm_orientation_offset is None:
			self.__forearm_orientation_offset = orientation
		
		# Rimuovo l'offset
		qi = quaternion_inverse(self.__forearm_orientation_offset)
		self.__forearm_orientation = quaternion_multiply(orientation, qi)
		
		# Aggiorno la pose del braccio del Nao se ho i dati di entrambi i Myo
		if self.__arm_orientation is not None:
			angles = self.calculate_angles(self.__arm_orientation, self.__forearm_orientation)
			self.publish_joints(self.__joint_names, angles)
		
	def sub_arm_imu(self, msg):
		# Quaterione iterabile
		orientation = quat_to_iterable(msg.orientation)
		
		# Riporto la rotazione con X+ verso il polso e Y+ verso l'interno, invece che X+ verso la spalla e Y+ verso l'esterno
		orientation = quaternion_multiply(quaternion_multiply(self.__q_to_ny, orientation), self.__q_to_ny)
		
		# Verifico se e' stato acquisito l'offset
		if self.__arm_orientation_offset is None:
			self.__arm_orientation_offset = orientation
		
		# Rimuovo l'offset
		qi = quaternion_inverse(self.__arm_orientation_offset)
		self.__arm_orientation = quaternion_multiply(orientation, qi)
		
		# Aggiorno la pose del braccio del Nao se ho i dati di entrambi i Myo
		if self.__forearm_orientation is not None:
			angles = self.calculate_angles(self.__arm_orientation, self.__forearm_orientation)
			self.publish_joints(self.__joint_names, angles)
	
	def draw_markers(self, arm_orient, forearm_orient, arm_id=0, forearm_id=1, arm_length=0.3, forearm_length=0.3, arm_color=ColorRGBA(1, 1, 1, 1), forearm_color=ColorRGBA(1, 0, 0, 1)):
		if arm_orient is None or forearm_orient is None:
			return
		
		ma = MarkerArray()
		
		# Arm: default ~30 cm length
		m_arm = default_marker()
		m_arm.id = arm_id
		m_arm.pose.orientation = Quaternion(*arm_orient)
		m_arm.scale = Vector3(x=arm_length, y=arm_length / 10., z=arm_length / 10.)
		m_arm.color = arm_color
		
		ma.markers.append(m_arm)
		
		# Forearm: default ~30 cm length
		m_forearm = default_marker()
		m_forearm.id = forearm_id
		m_forearm.pose.position = vect_rotation(Vector3(x=arm_length, y=0.0, z=0.0), arm_orient)
		m_forearm.pose.orientation = Quaternion(*forearm_orient)
		m_forearm.scale = Vector3(x=forearm_length, y=forearm_length / 10., z=forearm_length / 10.)
		m_forearm.color = forearm_color
		
		ma.markers.append(m_forearm)
		
		self.__pub_markers.publish(ma)
		
	def run(self):
		rospy.spin()
